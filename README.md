# Hamiltonian Travelling Salesman Problem

In this repository, we try to solve the classic salesman problem by mapping it to a QUBO problem and then to an quantum Ising problem, which is solved by finding its ground state with Variational Monte Carlo.

The map to a QUBO problem is central to a big possible speedup of the algorithm. Each QUBO variable is ultimately mapped to a qubit, so the fewer QUBO variables are used, the faster (exponentially) can VMC explore the physical problem Hilbert space.


## Experiments with PostgreSQL database support

When installing the PostgreSQL server, you need to create a user with a password. This is easy todo:

```bash
su postgres  # to get into the postgres user
psql -U postgres  # you can ignore the -U postgres part since you are already in the postgres user
\password  # set to mypass
```

It might also be needed to do the following installations:

```bash
pip install psycopg2==2.7.5
pip install sqlalchemy-utils
```

If there is a database and you want to delete it:

```bash
su postgres
psql
\l  # to list the databases
DROP DATABASE "name_of_some_database";
```
