import jax
from jax.experimental import stax
from netket.machine import Jax

def SumFiltersLayer():
    def init_fun(rng, input_shape):
        print("input_shape:::", input_shape)
        output_shape = (-1, input_shape[1], 1)
        return output_shape, ()
    
    def apply_fun(params, inputs, **kwargs):
        return inputs.sum(axis=-1)
    
    return init_fun, apply_fun

#We define a custom layer that performs the sum of its inputs 
def SumLayer():
    def init_fun(rng, input_shape):
        print("input_shape: ", input_shape)
        output_shape = (-1, 1)
        return output_shape, ()

    def apply_fun(params, inputs, **kwargs):
        return inputs.sum(axis=-1)

    return init_fun, apply_fun

def JaxFFNN(n_layers, n_hidden, hilbert):
    """
    """
    #TODO: add documentation
    layers = [stax.Dense(n_hidden), stax.Tanh] * n_layers
    model = stax.serial(*layers, SumLayer())
    ma = Jax(hilbert, model, dtype=complex)
    return ma
