import jax
from neural_tangents import stax as ntstax
from jax.experimental import stax
from netket.machine import Jax
from .ffnn import SumLayer, SumFiltersLayer
import functools
import math

def NetKetReshape(output_shape):
    def init_fun(rng, input_shape):
        return output_shape, ()
    def apply_fun(params, inputs, **kwargs):
        """
        There are two cases from netket. The first one
        is when inputs (i.e. samples of configurations)
        come in the shape (-1, W), where -1 indicates the
        dimension assigned to batches, and W is the width
        or the size of each configuration. Other shape is (W,).
        Recommended output_shape is (-1, W, 1), where the first
        index denotes batches, the second denotes the width,
        and the third denotes the number of channels.
        """
        return jax.numpy.reshape(inputs.astype(jax.numpy.complex128), output_shape)
    return init_fun, apply_fun

def JaxCNN(n_channels, n_layers, filter_size, stride, hilbert, padding='CIRCULAR'):
    """
    Creates a neural network with convolutional layers followed by a
    dense layer that sums the outputs.

    Parameters
    ----------
    n_channels: int or List[int]
        The number of channels of the each convolutional layer.
    n_layers: int
        The number of convolutional layers to be applied
    filter_size: int or List[int]
        The filter size of each convolutional layer
        (all convolutional layers are the same).
    stride: int or List[int]
        The stride of each convolutional layer
        (all convolutional layers are the same).
    hilbert: netket.hilbert
        A hilbert space for the physical system.
    padding: str
        Padding can be VALID or SAME.
        If VALID, only the number of convolutions defined by
        the input_size, the filter size and the stride will be
        applied. If SAME, there will be padding to accomodate
        one more convolution, if needed.

    Returns
    -------
    netket.machine.Jax
        A Jax machine from netket.
    """
    input_size = hilbert.size
    output_width = input_size
    if isinstance(filter_size, list):
        assert filter_size[0] <= output_width
    elif isinstance(filter_size, int):
        assert filter_size <= output_width
    padding_round = math.floor if padding == 'VALID' else math.ceil if padding == 'SAME' or padding == "CIRCULAR" else None
    if padding_round is None: raise NotImplementedError('Padding must be either VALID or SAME')
    if isinstance(stride, list):
        assert len(stride) == n_layers
    elif isinstance(stride, int):
        stride = [stride for i in range(n_layers)]
    if isinstance(filter_size, list):
        assert len(filter_size) == n_layers
    elif isinstance(filter_size, int):
        filter_size = [filter_size for i in range(n_layers)]
    if isinstance(n_channels, list):
        assert len(n_channels) == n_layers
    elif isinstance(n_channels, int):
        n_channels = [n_channels for i in range(n_layers)]

    # Check that the convolutional neural network will produce something useful.
    for _i in range(n_layers - 1):
        output_width = padding_round((output_width - filter_size[_i])/stride[_i]) + 1
        assert filter_size[_i+1] <= output_width
    cnn_layers = []
    for _i in range(n_layers):
        cnn_layers.append(ntstax.Conv(n_channels[_i], filter_shape=(filter_size[_i],), strides=(stride[_i],), padding=padding))
        cnn_layers.append(stax.Relu)
    model = stax.serial(
        NetKetReshape((-1, input_size, 1)),
        *cnn_layers,
        SumFiltersLayer(),
        stax.Flatten,
        stax.Dense(1),
        SumLayer(),
    )
    ma = Jax(hilbert, model, dtype=complex)
    return ma
