"""
This file implements a local + exchange sampler for netket
"""

import numpy as _np
from netket import random as _random
from numba import jit, int64, float64
from numba.experimental import jitclass
from functools import singledispatch
from netket.sampler import MetropolisHastings
import jax

class _JaxEchangeSitesKernel_class:
    def __init__(self, machine, length, d_matrix, totally_random):
        self.size = machine.input_size
        self.trials = 5
        self.length = length
        self.d_matrix = d_matrix
        self.num_cities = self.size
        self.random = totally_random

    def transition(self, rng, state):
        rng1, rng2 = jax.random.split(rng, 2)

        si = jax.random.randint(rng1, shape=(1,), minval=0, maxval=self.num_cities)
        sj = jax.random.randint(rng2, shape=(1,), minval=1, maxval=self.length + 1)
        sj = jax.numpy.mod(si + sj, self.num_cities)

        state_1 = jax.ops.index_update(state, si, state[sj])
        transition_state = jax.ops.index_update(state_1, sj, state[si])
        ordered_state = self.sort_state(transition_state)
        return ordered_state

    def sort_state(self, state):
        idx = jax.lax.argmin(state, axis=0, index_dtype=_np.int32)
        return jax.numpy.roll(state, -idx)

    def random_state(self, key, state):
        """
        Creates a random state where every site occupation is different,
        going from 0 to state.shape[1]-1.
        """
        if self.random:
            cities = jax.numpy.arange(self.num_cities)
            cities = jax.random.shuffle(key, cities)
        else:
            if self.d_matrix is not None:
                cities = [0]
                for i in range(self.d_matrix.shape[1] - 1):
                    col = self.d_matrix[:, cities[-1]]
                    idx = _np.argsort(col)[::-1]
                    for j in idx:
                        if j not in cities:
                            cities.append(j)
                            break
            else:
                cities = _np.arange(self.num_cities)
                _np.random.shuffle(cities)
        newstate = jax.numpy.asarray(cities)
        return key, newstate


@jitclass(
    [
        ("size", int64),
        ("trials", int64),
        ("spins", int64),
        ("num_cities", int64),
        ("length", int64),
        ("d_matrix", float64[:,:]),
        ("random", int64)
    ]
)
class _ExchangeSitesKernel_class:
    def __init__(self, size, spins, length, d_matrix, totally_random):
        self.size = size
        self.trials = 5
        self.length = length
        self.d_matrix = d_matrix
        if spins:
            self.spins = 1
            self.num_cities = int64(_np.sqrt(self.size))
        else:
            self.spins = 0
            self.num_cities = self.size
        if totally_random:
            self.random = 1
        else:
            self.random = 0
        

    def transition(self, state, state_1, log_prob_corr):
        if self.spins:
            self.transition_spins(state, state_1, log_prob_corr)
        else:
            self.transition_nlevels(state, state_1, log_prob_corr)

    def transition_nlevels(self, state, state_1, log_prob_corr):
        for i in range(state.shape[0]):
            state_1[i] = state[i]  # -> [1,5,4,3,2,0]
            for _trial in range(self.trials):
                si = _random.randint(0, self.num_cities)
                sj = _random.randint(1, self.length + 1)
                sj = (si + sj) % state.shape[1]
                if state[i, sj] != state[i, si]:
                    # [1,5X,4,3X,2,0] -> state[i]
                    state_1[i, si] = state[i, sj]
                    state_1[i, sj] = state[i, si]
                    # [1,3,4,5,2,0] -> state_1[i]
                    break

        # Lexsort state:
        # [1,3,4,5,2,0] -> state_1[i]
        self.sort_state(state_1)
        # [0,1,3,4,5,2] -> state_1[i]

        log_prob_corr.fill(0.0)

    def transition_spins(self, state, state_1, log_prob_corr):
        state_aux = self.spins2city(state)  # [0, 1, 0, 1, 0, 0, 0, 0, 1] -> [1, 0, 2]
        state_1_aux = self.spins2city(state_1)
        self.transition_nlevels(state_aux, state_1_aux, log_prob_corr)
        state_aux = self.city2spins(
            state_aux
        )  # [1, 0, 2] -> [0, 1, 0, 1, 0, 0, 0, 0, 1]
        state_1_aux = self.city2spins(state_1_aux)
        state[:] = state_aux[:]
        state_1[:] = state_1_aux[:]

    def sort_state(self, state):
        length = state.shape[1]
        for i in range(state.shape[0]):
            statei = state[i]
            rolls = _np.zeros((length, length))
            for j in range(length):
                rolls[j, :] = _np.roll(statei, j)
            for col in range(length):
                column = rolls[:, col]
                rolls = rolls[_np.argwhere(column == _np.min(column)).flatten()]
                if rolls.shape[0] == 1:
                    break
            state[i] = rolls[0]

    def city2spins(self, state):
        spinstate = _np.zeros((state.shape[0], self.size))
        for i in range(state.shape[0]):
            statei = state[i]
            # single spin state
            sstate = _np.ones((self.num_cities, self.num_cities))
            for j in range(statei.size):
                sstate[j, int64(statei[j])] = -1
            spinstate[i] = sstate.flatten()
        return spinstate

    def spins2city(self, state):
        citystate = _np.zeros((state.shape[0], self.num_cities))
        for i in range(state.shape[0]):
            statei = state[i].reshape(self.num_cities, self.num_cities)
            # single city state
            cstate = _np.zeros(self.num_cities)
            for j in range(statei.shape[0]):
                cstate[j] = _np.argmin(
                    statei[j]
                )  # argmin because all spins are up, except for excitations
            citystate[i] = cstate
        return citystate

    def random_state(self, state):
        """
        Creates a random state where every site occupation is different,
        going from 0 to state.shape[1]-1.
        """
        if self.random:
            cities = _np.arange(self.num_cities)
            for i in range(state.shape[0]):
                _np.random.shuffle(cities)
                if self.spins:
                    st = self.city2spins(_np.copy(cities).reshape(1, -1))
                    state[i] = st[0]
                else:
                    st = _np.copy(cities)
                    state[i] = st
        else:
            if self.d_matrix is not None:
                cities = [0]
                for i in range(self.d_matrix.shape[1] - 1):
                    col = self.d_matrix[:, cities[-1]]
                    idx = _np.argsort(col)[::-1]
                    for j in idx:
                        if j not in cities:
                            cities.append(j)
                            break
                cities = _np.asarray(cities)
            else:
                cities = _np.arange(self.num_cities)
                _np.random.shuffle(cities)

            if self.spins:
                cities = self.city2spins(cities.reshape(1, -1))
                for i in range(state.shape[0]):
                    state[i] = cities[0]
            else:
                for i in range(state.shape[0]):
                    state[i] = cities


@singledispatch
def _ExchangeSitesKernel(machine, spins, length, d_matrix, totally_random):
    return _ExchangeSitesKernel_class(machine.input_size, spins, length, d_matrix, totally_random)


def MetropolisLocalExchangeSites(
    machine, hamiltonian=None, n_chains=16, sweep_size=None, spins=False, length=2, d_matrix=None, totally_random=False,
):
    """"""
    # TODO: add function documentation
    if spins:
        num_cities = _np.sqrt(machine.input_size)
    else:
        num_cities = machine.input_size
    
    assert length + 1 <= num_cities
    return MetropolisHastings(
        machine, _ExchangeSitesKernel(machine, spins, length, d_matrix, totally_random), n_chains, sweep_size
    )

def JaxMetropolisLocalExchangeSites(machine, hamiltonian=None, n_chains=16, sweep_size=None, spins=False, length=2, d_matrix=None, totally_random=False):
    """
    """ # TODO add documentation
    if spins:
        raise ValueError("Jax Machines and Samplers are built for N N-Level systems only")
    else:
        num_cities = machine.input_size
    assert length + 1 <= num_cities
    return MetropolisHastings(machine, _JaxEchangeSitesKernel_class(machine, length, d_matrix, totally_random), n_chains, sweep_size)