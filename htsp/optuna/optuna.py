import optuna
import netket as nk
import numpy as np
import pandas as pd
import os
from htsp.solver import Ising, NLevels
from htsp.machine import JaxCNN
from htsp.sampler import JaxMetropolisLocalExchangeSites
from scipy.spatial import distance_matrix
from contextlib import redirect_stdout, redirect_stderr
from datetime import date
import hashlib
from memory_profiler import memory_usage
from collections import OrderedDict
import traceback


class Tuner:
    """"""
    # TODO add documentation
    def __init__(self, solver, optuna_params, city_positions=None, d_matrix=None, machine=JaxCNN, sampler=JaxMetropolisLocalExchangeSites, 
    optimiser=nk.optimizer.Sgd, patience=100, baseline=None, timeout=600, min_delta=0, database='sqlite'):
        """
        Creates a `Tuner` that uses optuna to find the best hyperparameters.

        Parameters
        ----------
        solver: str
            A Hamiltonian solver for the salesman problem (**not initialised**)
            At the moment only RBM based solvers are supported. If 'ising' it
            loads the spin based solver (large). If 'nlevels' it loads the
            n-level systems-based solver (medium).
        optuna_params: dict[tuple, dict]
            Dictionaries whose keys are tuples of the form (name_kwargs, 
            name_variable), e.g. ("machine_kwargs", "n_hidden") or 
            (None, "epochs") and whose value is a float value or another 
            dictionary with the keys:
            - suggest: the name of the suggesting function from optuna,
            e.g. 'suggest_int'.
            - limits: a tuple of the form (minimum, maximum)
        city_positions: array-like, optional
            A 2D numpy array, where each row is the coordinate of a city.
            A distance matrix will be calculated through the euclidean
            distance between the coordinates of the cities.
        d_matrix: array-like, optional
            A 2D numpy array with the distance matrix.
        """
        self.solver_name = solver
        if solver == "ising":
            self.solver = Ising
            self.spins = True
        elif solver == "nlevels":
            self.solver = NLevels
            self.spins = False
        else:
            raise ValueError(
                f"Solver {solver} is not supported. Options are 'ising' or 'nlevels'."
            )
        self.optuna_params = optuna_params
        if d_matrix is None:
            assert city_positions is not None
            self.city_positions = city_positions
            self.d_matrix = self.get_d_matrix()
        else:
            self.city_positions = None
            self.d_matrix = d_matrix
        self.num_cities = self.d_matrix.shape[0]
        self.baseline = baseline
        self.patience = patience
        self.timeout = timeout
        self.machine = machine
        self.sampler = sampler
        self.optimiser = optimiser
        self.min_delta = min_delta
        self.database = database
        

    def get_d_matrix(self):
        """"""
        # TODO add documentation
        assert len(self.city_positions.shape) == 2
        d_matrix = distance_matrix(self.city_positions, self.city_positions)
        return d_matrix

    def __solver_wrapper(
        self,
        total_samples,
        epochs,
        machine_kwargs,
        sampler_kwargs,
        optimizer_kwargs,
        print_states=False,
    ):
        n_samples = total_samples // sampler_kwargs['n_chains']
        name = hashlib.sha256(
            f"{total_samples}{epochs}{machine_kwargs}{sampler_kwargs}{optimizer_kwargs}".encode()
        ).hexdigest()
        solver = self.solver(
            num_cities=self.num_cities,
            d_matrix=self.d_matrix,
            optimizer=self.optimiser,
            n_samples=n_samples,
            machine=self.machine,
            sampler=self.sampler,
            machine_kwargs=machine_kwargs,
            sampler_kwargs=sampler_kwargs,
            optimizer_kwargs=optimizer_kwargs,
        )

        mem_evol = memory_usage(
                    (
                        solver.run,
                        (),
                        {"epochs": epochs, "filename": name, "patience": self.patience, "baseline": self.baseline, "timeout": self.timeout, "min_delta": self.min_delta},
                    )
                )
        energy = solver.get_lowest_distance()
        solver.clean_up(all=False)
        if print_states:
            solver.get_relevant_states(num_states=10)

        return energy, mem_evol, name

    def __pseudo_objective(self, trial):
        """"""
        # TODO add documentation
        suggestions = {
            "suggest_int": trial.suggest_int,
            "suggest_loguniform": trial.suggest_loguniform,
            "suggest_uniform": trial.suggest_uniform,
        }

        params = {}

        machine_kwargs = OrderedDict()
        sampler_kwargs = OrderedDict()
        optimizer_kwargs = OrderedDict()

        for param_key, param_value in self.optuna_params.items():
            kwargs, param = param_key
            if isinstance(param_value, dict):
                suggestion = suggestions[param_value["suggest"]]
                low, hig = param_value["limits"]
            else:
                # This is just a workaround not to change code afterwards:
                suggestion = lambda name, x, y: x
                low, hig = param_value, None
            if kwargs == "machine_kwargs":
                machine_kwargs[param] = suggestion(param, low, hig)
            elif kwargs == "sampler_kwargs":
                sampler_kwargs[param] = suggestion(param, low, hig)
            elif kwargs == "optimizer_kwargs":
                optimizer_kwargs[param] = suggestion(param, low, hig)
            elif kwargs is None:
                params[param] = suggestion(param, low, hig)
        params["machine_kwargs"] = machine_kwargs
        params["sampler_kwargs"] = sampler_kwargs
        params["optimizer_kwargs"] = optimizer_kwargs
        try:
            energy, mem_evol, name = self.__solver_wrapper(**params)
            trial.set_user_attr('memory_peak', np.max(mem_evol))
            trial.set_user_attr('filename', name)
            return energy
        except Exception as e:
            # TODO: a better treatment of errors can be done by passing them to the optuna study (it will handle them automatically)
            # Also this is dangerous because errors in htsp might be overlooked because of how this is handled!
            print(traceback.format_exc())
            return 1e6

    def __optimise(self, study_name, filepath, n_trials=100, load_if_exists=False):
        """"""
        # TODO add documentation
        if self.database != 'postgres' and self.database != 'sqlite':
            raise ValueError(f'database should be postgres or sqlite, not {self.database}')

        study = optuna.create_study(
            study_name=study_name,
            storage=filepath,
            direction="minimize",
            load_if_exists=load_if_exists,
        )
        study.optimize(self.__pseudo_objective, n_trials=n_trials, gc_after_trial=True, callbacks=[lambda study, trial: self.__max_trial_callback(study, trial, n_trials)])

    def __max_trial_callback(self, study, trial, n_trials):
        n_complete = len([t for t in study.trials if t.state == optuna.trial.TrialState.COMPLETE or t.state == optuna.trial.TrialState.RUNNING])
        if n_complete >= n_trials:
            study.stop()


    def run_experiment(self, study_name, filepath, results_directory, n_trials=100, load_if_exists=False):
        """"""
        # TODO add documentation
        if not os.path.isdir(results_directory):
            os.mkdir(results_directory)

        contents = ""
        contents += f"Experiment name: {study_name}\n"
        contents += f"Date: {str(date.today())}\n"
        contents += f"Solver: {self.solver_name}\n"
        contents += f"Number of cities: {self.num_cities}\n"
        contents += "Parameters:\n"

        for (kwargs, param), suggestion in self.optuna_params.items():
            if kwargs is None:
                kwargs = "Independent"
            if isinstance(suggestion, dict):
                sug = suggestion["suggest"]
                low, hig = suggestion["limits"]
                contents += f"    {param} ==> Type: {kwargs} Suggestion: {sug} Limits: {low}-{hig}\n"
            else:
                contents += f"    {param} ==> Type: {kwargs} Fixed value: {suggestion}\n"

        contents += f"Distance matrix:\n{self.d_matrix}"

        with open(os.path.join(results_directory, "parameters.log"), "w") as f:
            f.write(contents)

        with open(os.path.join(results_directory, "experiment.log"), "w") as f:
            with redirect_stdout(f), redirect_stderr(f):
                self.__optimise(study_name, filepath, n_trials=n_trials, load_if_exists=load_if_exists)

    def read_experiment(self, studyname, studypath):
        """
        Converts an optuna database into a dataframe to analyse the results.

        Parameters
        ----------
        studyname: str
            The name given to the study.
        studypath: str
            The absolute path to the study, normally of the form 
            sqlite:///[studyname].db

        Returns
        -------
        pandas.DataFrame
            A dataframe containing the information of the study.
        """

        kwargs_list = ["machine_kwargs", "sampler_kwargs", "optimizer_kwargs"]
        def get_param_value(key, dict_, alternative):
            if key in dict_.keys():
                return dict_[key]
            else:
                return alternative[key]
        def process_trial(trial):
            record = {}
            record["value"] = trial.value
            try:
                record["runtime"] = (trial.datetime_complete - trial.datetime_start).total_seconds()
            except:
                record["runtime"] = np.nan
            for key, value in trial.params.items():
                record[key] = value
            if "memory_peak" in trial.user_attrs.keys():
                record["memory_peak"] = trial.user_attrs["memory_peak"]
            else:
                record["memory_peak"] = np.nan
            if "filename" in trial.user_attrs.keys():
                record["filename"] = trial.user_attrs["filename"]
            else:
                record["filename"] = np.nan
            record["final_state"] = str(trial.state).split('.')[-1]

            return record

        study = optuna.study.load_study(study_name=studyname,  storage=studypath)
        records = []
        for trial in study.trials:
            records.append(process_trial(trial))

        df = pd.DataFrame(records)
        return df
