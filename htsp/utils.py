import numpy as np


def lexsort_based(data):
    """
    Deletes duplicate rows of a matrix
    based on https://stackoverflow.com/a/31097280/4107770
    """
    sorted_data = data[np.lexsort(data.T), :]
    row_mask = np.append([True], np.any(np.diff(sorted_data, axis=0), 1))
    return sorted_data[row_mask]
