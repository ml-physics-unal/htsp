from netket.operator import AbstractOperator
from netket.hilbert import Boson
import numpy as np
from numba import jit, int64

class TSPNL(AbstractOperator):
    """
    An efficient operator to express the Hamiltonian of
    an N-Level system corresponding to a travelling salesman
    problem with N cities.
    """

    def __init__(self, graph, hilbert, num_cities, d_matrix):
        """
        Constructs the N-Level system hamiltonian for the 
        travelling salesman problem for N cities.

        Parameters
        ----------
        graph: nk.graph
            A graph from netket
        hilbert: nk.hilbert
            A hilbert space from netket
        num_cities: int
            The number of cities
        d_matrix: array-like of shape (num_cities, num_cities)
            The distance matrix between the cities
        """
        self._hilbert = hilbert
        self._num_cities = num_cities
        self._d_matrix = d_matrix

        assert isinstance(hilbert, Boson)

        self._n_max = hilbert.n_max
        self._n_sites = hilbert.size
        self._edges = np.asarray(list(graph.edges()))
        self._max_conn = 1
        self._max_mels = np.empty(self._max_conn, dtype=np.complex128)
        self._max_xprime = np.empty((self._max_conn, self._n_sites))

        super().__init__()

    @property
    def hilbert(self):
        r"""AbstractHilbert: The hilbert space associated to this operator."""
        return self._hilbert

    @property
    def size(self):
        return self._hilbert.size

    def get_conn(self, x):
        r"""Finds the connected elements of the Operator. Starting
        from a given quantum number x, it finds all other quantum numbers x' such
        that the matrix element :math:`O(x,x')` is different from zero. In general there
        will be several different connected states x' satisfying this
        condition, and they are denoted here :math:`x'(k)`, for :math:`k=0,1...N_{\mathrm{connected}}`.
        This is a batched version, where x is a matrix of shape (batch_size,hilbert.size).
        Args:
            x (array): An array of shape (hilbert.size) containing the quantum numbers x.
        Returns:
            matrix: The connected states x' of shape (N_connected,hilbert.size)
            array: An array containing the matrix elements :math:`O(x,x')` associated to each x'.
        """
        mels = self._max_mels
        x_prime = self._max_xprime

        mels[0] = 0.0
        x_prime[0] = np.copy(x)
        n_max = self._n_max
        for edge_index in range(self._n_sites):
            i, j = x[edge_index], x[(edge_index + 1)%self._n_sites]
            mels[0] += self._d_matrix[int64(i), int64(j)]

        return np.copy(x_prime), np.copy(mels)

    @staticmethod
    @jit(nopython=True)
    def _flattened_kernel(
        x, sections, edges, mels, x_prime, n_max, max_conn, d_matrix
    ):
        batch_size = x.shape[0]
        n_sites = x.shape[1]

        if mels.size < batch_size * max_conn:
            mels = np.empty(batch_size * max_conn, dtype = np.complex128)
            x_prime = np.empty((batch_size * max_conn, n_sites))

        diag_ind = 0
        for b in range(batch_size):
            mels[diag_ind] = 0.0
            x_prime[diag_ind] = np.copy(x[b])

            odiag_ind = 1 + diag_ind
            for edge_index in range(n_sites):
                i, j = x[b, edge_index], x[b, (edge_index + 1)%n_sites]
                mels[diag_ind] += d_matrix[int64(i), int64(j)]
            # TODO: Test if this leads to better convergence:
            # mels[diag_ind] = np.log(mels[diag_ind])

            diag_ind = odiag_ind
            sections[b] = odiag_ind

        return np.copy(x_prime[:odiag_ind]), np.copy(mels[:odiag_ind])


    def get_conn_flattened(self, x, sections):
        r"""Finds the connected elements of the Operator. Starting
        from a given quantum number x, it finds all other quantum numbers x' such
        that the matrix element :math:`O(x,x')` is different from zero. In general there
        will be several different connected states x' satisfying this
        condition, and they are denoted here :math:`x'(k)`, for :math:`k=0,1...N_{\mathrm{connected}}`.
        This is a batched version, where x is a matrix of shape (batch_size,hilbert.size).
        Args:
            x (matrix): A matrix of shape (batch_size,hilbert.size) containing
                        the batch of quantum numbers x.
            sections (array): An array of size (batch_size) useful to unflatten
                        the output of this function.
                        See numpy.split for the meaning of sections.
        Returns:
            matrix: The connected states x', flattened together in a single matrix.
            array: An array containing the matrix elements :math:`O(x,x')` associated to each x'.
        """
        return self._flattened_kernel(
            x,
            sections,
            self._edges,
            self._max_mels,
            self._max_xprime,
            self._n_max,
            self._max_conn,
            self._d_matrix,
        )


