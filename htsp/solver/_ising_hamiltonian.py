from netket.operator import AbstractOperator
from netket.hilbert import Spin
import numpy as np
from numba import jit, int64

@jit(nopython=True)
def spins2city(state, n_cities):
    citystate = np.zeros(n_cities, dtype=int64)
    statei = state.reshape(n_cities, n_cities)
    for j in range(statei.shape[0]):
        citystate[j] = np.argmin(statei[j])
    return citystate

@jit(nopython=True)
def iα2index(i, α, n_cities):
    return i + α * n_cities

@jit(nopython=True)
def compute_energy_QUBO(state, n_cities, d_matrix):
    energy = 0
    for α in range(n_cities):
        for i in range(n_cities):
            for j in range(n_cities):
                σiα = state[iα2index(i, α, n_cities)]
                σjαp1 = state[iα2index(j, (α + 1)%n_cities, n_cities)]
                energy += 0.25 * d_matrix[i, j] * (σiα * σjαp1 - σiα - σjαp1 + 1)
    return energy


class TSPIsing(AbstractOperator):
    """
    An efficient operator to express the Hamiltonian of
    an Ising system corresponding to a travelling salesman
    problem with N cities.
    """

    def __init__(self, graph, hilbert, num_cities, d_matrix, _type="QUBO"):
        """
        Constructs the Ising system hamiltonian for the 
        travelling salesman problem for N cities.

        Parameters
        ----------
        graph: nk.graph
            A graph from netket
        hilbert: nk.hilbert
            A hilbert space from netket
        num_cities: int
            The number of cities
        d_matrix: array-like of shape (num_cities, num_cities)
            The distance matrix between the cities
        _type: str
            Either "QUBO" or "Energy"
        """
        self._hilbert = hilbert
        self._num_cities = num_cities
        self._d_matrix = d_matrix

        assert isinstance(hilbert, Spin)

        self._n_sites = hilbert.size
        self._n_cities = int64(np.sqrt(self._n_sites))
        self._edges = np.asarray(list(graph.edges()))
        self._max_conn = 1
        self._max_mels = np.empty(self._max_conn, dtype=np.complex128)
        self._max_xprime = np.empty((self._max_conn, self._n_sites))
        if _type == "QUBO":
            self._type = 0
        elif _type == "Energy":
            self._type = 1
        else:
            raise ValueError(f"Type {_type} not supported. Select from 'QUBO' or 'Energy'.")

        super().__init__()

    @property
    def hilbert(self):
        r"""AbstractHilbert: The hilbert space associated to this operator."""
        return self._hilbert

    @property
    def size(self):
        return self._hilbert.size


    def get_conn(self, x):
        r"""Finds the connected elements of the Operator. Starting
        from a given quantum number x, it finds all other quantum numbers x' such
        that the matrix element :math:`O(x,x')` is different from zero. In general there
        will be several different connected states x' satisfying this
        condition, and they are denoted here :math:`x'(k)`, for :math:`k=0,1...N_{\mathrm{connected}}`.
        This is a batched version, where x is a matrix of shape (batch_size,hilbert.size).
        Args:
            x (array): An array of shape (hilbert.size) containing the quantum numbers x.
        Returns:
            matrix: The connected states x' of shape (N_connected,hilbert.size)
            array: An array containing the matrix elements :math:`O(x,x')` associated to each x'.
        """
        mels = self._max_mels
        x_prime = self._max_xprime

        mels[0] = 0.0
        x_prime[0] = np.copy(x)
        if self._type:
            x_citystate = spins2city(x)
            for edge_index in range(self._n_cities):
                i, j = x_citystate[edge_index], x_citystate[(edge_index + 1)%self._n_cities]
                mels[0] += self._d_matrix[int64(i), int64(j)]
        else:
            mels[0] += compute_energy_QUBO(x, self._n_cities, self._d_matrix)

        return np.copy(x_prime), np.copy(mels)

    @staticmethod
    @jit(nopython=True)
    def _flattened_kernel(
        x, sections, edges, mels, x_prime, max_conn, d_matrix, n_cities, _type,
    ):
        batch_size = x.shape[0]
        n_sites = x.shape[1]

        if mels.size < batch_size * max_conn:
            mels = np.empty(batch_size * max_conn, dtype = np.complex128)
            x_prime = np.empty((batch_size * max_conn, n_sites))

        diag_ind = 0
        for b in range(batch_size):
            mels[diag_ind] = 0.0
            x_prime[diag_ind] = np.copy(x[b])

            odiag_ind = 1 + diag_ind

            if _type:
                x_citystate = spins2city(x[b], n_cities)
                for edge_index in range(n_cities):
                    i, j = x_citystate[edge_index], x_citystate[(edge_index + 1)%n_cities]
                    mels[diag_ind] += d_matrix[int64(i), int64(j)]
            else:
                mels[diag_ind] += compute_energy_QUBO(x[b], n_cities, d_matrix)

            diag_ind = odiag_ind
            sections[b] = odiag_ind

        return np.copy(x_prime[:odiag_ind]), np.copy(mels[:odiag_ind])


    def get_conn_flattened(self, x, sections):
        r"""Finds the connected elements of the Operator. Starting
        from a given quantum number x, it finds all other quantum numbers x' such
        that the matrix element :math:`O(x,x')` is different from zero. In general there
        will be several different connected states x' satisfying this
        condition, and they are denoted here :math:`x'(k)`, for :math:`k=0,1...N_{\mathrm{connected}}`.
        This is a batched version, where x is a matrix of shape (batch_size,hilbert.size).
        Args:
            x (matrix): A matrix of shape (batch_size,hilbert.size) containing
                        the batch of quantum numbers x.
            sections (array): An array of size (batch_size) useful to unflatten
                        the output of this function.
                        See numpy.split for the meaning of sections.
        Returns:
            matrix: The connected states x', flattened together in a single matrix.
            array: An array containing the matrix elements :math:`O(x,x')` associated to each x'.
        """
        return self._flattened_kernel(
            x,
            sections,
            self._edges,
            self._max_mels,
            self._max_xprime,
            self._max_conn,
            self._d_matrix,
            self._n_cities,
            self._type
        )
