from . import AbstractSolver, TSPNL
from htsp.utils import lexsort_based
import networkx as nx
import netket as nk
import numpy as np
import os
from math import factorial
from htsp.sampler import MetropolisLocalExchangeSites


class NLevels(AbstractSolver):
    """"""

    # TODO: add class documentation
    def __init__(
        self,
        num_cities,
        d_matrix,
        machine=nk.machine.RbmMultiVal,
        sampler=MetropolisLocalExchangeSites,
        optimizer=nk.optimizer.Sgd,
        diag_shift=0.1,
        n_samples=100,
        machine_kwargs={"alpha": 1},
        sampler_kwargs={"n_chains": 4, "sweep_size": 4},
        optimizer_kwargs={"learning_rate": 0.005},
        seed=0,
    ):
        """"""
        # TODO: add function documentation
        nk.random.seed(seed)
        np.random.seed(seed)
        self.__num_cities = num_cities
        self.__d_matrix = d_matrix

        g = nx.complete_graph(self.__num_cities)
        self.graph = nk.graph.NetworkX(g)
        n_bosons = self.__num_cities * (self.__num_cities - 1) * 0.5
        self.hilbert = nk.hilbert.Boson(
            graph=self.graph, n_max=self.__num_cities - 1, n_bosons=n_bosons
        )
        self.hamiltonian = TSPNL(self.graph, self.hilbert, self.__num_cities, self.__d_matrix)
        # self.hamiltonian = nk.operator.LocalOperator(self.hilbert)
        # self.__build_hamiltonian()
        self.machine = machine(hilbert=self.hilbert, **machine_kwargs)
        self.machine.init_random_parameters(seed=seed)
        try:
            self.sampler = sampler(
                machine=self.machine, hamiltonian=self.hamiltonian, d_matrix=self.__d_matrix, **sampler_kwargs
            )
        except:
            self.sampler = sampler(machine=self.machine, d_matrix=self.__d_matrix, **sampler_kwargs)
        if True:# isinstance(optimizer, nk.optimizer.Sgd): #TODO: see how to check this condition
            self.optimizer = optimizer(self.machine, **optimizer_kwargs)
        else:
            raise ValueError("Only sgd is supported as an optimizer for now")
        self.sr = nk.optimizer.SR(self.machine, diag_shift=diag_shift)
        self.vmc = nk.Vmc(
            hamiltonian=self.hamiltonian,
            sampler=self.sampler,
            optimizer=self.optimizer,
            n_samples=n_samples,
            sr=self.sr,
        )
        super().__init__()

    @property
    def num_cities(self):
        return self.__num_cities

    @property
    def hilbert_size(self):
        return factorial(self.num_cities)

    @property
    def d_matrix(self):
        return self.__d_matrix

    def __build_hamiltonian(self):
        """"""  # TODO add documentation
        # Create a two-body operator to account for distances

        tbd = np.zeros((self.num_cities ** 2, self.num_cities ** 2))
        for i in range(self.num_cities):
            for j in range(self.num_cities):
                tbd[self.num_cities * i + j, self.num_cities * i + j] = self.d_matrix[
                    i, j
                ]

        # Hamiltonian that measures tour distance
        for i in range(self.num_cities):
            self.hamiltonian += nk.operator.LocalOperator(
                self.hilbert, tbd, [i, (i + 1) % self.num_cities]
            )

    def __inverse_one_hot(self, samples):
        """
        Performs the inverse one hot transformation.

        Parameters
        ----------
        samples: 2D numpy.array
            Each row is a one hot encoded sample, where 1 are excitations
            and -1 are not. For instance, if there is a maximum of 3 states
            per site (0 bosons, 1 boson, 2 bosons), then the configuration
            [1, 2] is one hot encoded to [-1, 1, -1, -1, -1, 1]
        """
        reshaped = samples.reshape(self.num_cities, self.num_cities, -1)
        return np.argmax(reshaped, axis=2)

    def get_relevant_states(self, num_states, num_samples=1000, add_true_sol=False):
        """"""
        # TODO: implement documentation

        # Create a bunch of samples (configurations)
        samples = np.array(list(self.sampler.samples(num_samples)))

        samples_r = samples.reshape((-1, samples.shape[-1]))
        unique_samples = lexsort_based(samples_r)

        # Add true solution
        if add_true_sol:
            new_sample = np.arange(self.num_cities)
            unique_samples = np.vstack((unique_samples, new_sample))

        # Get the wavefunction coefficients
        coeffs = self.machine.log_val(unique_samples)

        # Order configurations according to the energy
        energies = np.array([self.energy(config) for config in unique_samples])

        sorted_idx = np.argsort(energies)[:num_states]

        for config, coeff, energy in zip(
            unique_samples[sorted_idx], coeffs[sorted_idx], energies[sorted_idx]
        ):
            print("*-.-*-.-*-.-*-.-*-.-*-.-*-.-*-.-*-.-*")
            print(f"Configuration {config}")
            print(f"has relative log coefficient {coeff:.3f}")
            print(f"with energy {energy:.4f}.")

    def energy(self, conf):
        """"""
        # TODO: implement documentation
        expected_val = nk.operator.local_values(
            self.hamiltonian, self.machine, conf.reshape(1, -1)
        )[0]
        assert np.abs(np.imag(expected_val)) < 1e-10
        return np.real(expected_val)

    def get_lowest_energy(self):
        """"""
        # TODO: implement documentation
        samples = np.array(list(self.sampler.samples(1000)))

        samples_r = samples.reshape((-1, samples.shape[-1]))
        unique_samples = lexsort_based(samples_r)
        energies = [self.energy(config) for config in unique_samples]
        return min(energies)

    def get_lowest_distance(self):
        """"""
        # TODO: implement documentation

        return self.get_lowest_energy()

    def clean_up(self, all=True):
        """"""
        # TODO: finish documentation
        if self.name is not None:
            if all:
                os.remove(f"{self.name}.log")
            os.remove(f"{self.name}.wf")
