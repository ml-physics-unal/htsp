import abc
import netket as nk

class AbstractSolver(abc.ABC):
    """
    Abstract class for Solver objects
    """

    @property
    @abc.abstractmethod
    def num_cities(self):
        """
        int:
            The total number of cities
        """
        return NotImplementedError

    @property
    @abc.abstractmethod
    def hilbert_size(self):
        """
        int:
            The total number of configurations of the Hilbert space
        """
        return NotImplementedError

    def run(self, epochs=100, filename="test", patience=100, baseline=None, timeout=None, min_delta=0):
        """"""  # TODO add documentation
        self.name = filename
        callbacks = []
        if patience is not None or baseline is not None:
            es = nk.callbacks.EarlyStopping(patience=patience, baseline=baseline, min_delta=min_delta)
            callbacks.append(es)
        if timeout is not None:
            to = nk.callbacks.Timeout(timeout=timeout)
            callbacks.append(to)
        if len(callbacks) == 0:
            callbacks = None
            
        self.vmc.run(n_iter=epochs, out=filename, callback=callbacks) # pylint: disable=no-member
