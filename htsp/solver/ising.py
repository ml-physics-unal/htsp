from . import AbstractSolver, TSPIsing
from htsp.utils import lexsort_based
from htsp.sampler import MetropolisLocalExchangeSites
import networkx as nx
import netket as nk
import numpy as np
import json
import os


class Ising(AbstractSolver):
    """"""

    # TODO: add class documentation
    def __init__(
        self,
        num_cities,
        d_matrix,
        machine=nk.machine.RbmSpin,
        sampler=MetropolisLocalExchangeSites,
        optimizer=nk.optimizer.Sgd,
        diag_shift=0.1,
        n_samples=100,
        machine_kwargs={"alpha": 1},
        sampler_kwargs={"n_chains": 4, "sweep_size": 4, "spins": True},
        optimizer_kwargs={"learning_rate": 0.005},
        seed=0,
        symmetry=False,
        hamiltonian_type="Energy",
    ):
        """
        Creates an `Ising` solver, which essentially writes the salesman problem
        as a QUBO problem, defining some QUBO variables
        """
        # TODO: add function documentation
        nk.random.seed(seed)
        self.__num_cities = num_cities
        self.num_spins = self.num_cities ** 2
        self.__d_matrix = d_matrix
        self.name = None
        g = nx.complete_graph(self.num_spins)
        # netket initialises all spins in +1, excitations are -1
        total_spin = (self.num_spins - self.__num_cities*2)/2

        # Define netket problem
        self.graph = nk.graph.NetworkX(g)
        self.hilbert = nk.hilbert.Spin(s=0.5, graph=self.graph, total_sz=total_spin)
        self.hamiltonian = TSPIsing(self.graph, self.hilbert, self.__num_cities, self.__d_matrix, _type=hamiltonian_type)
        # self.hamiltonian = nk.operator.LocalOperator(self.hilbert)
        # self.__build_hamiltonian()
        if symmetry:
            canonical_nodes = np.arange(self.num_spins, dtype=int)
            symmetry = [canonical_nodes]
            for i in range(1, self.__num_cities):
                symmetry.append(np.roll(canonical_nodes, i * self.__num_cities))
            mirror_canonical_nodes = np.empty(canonical_nodes.shape, dtype=int)
            for i in range(self.__num_cities):
                mirror_canonical_nodes[i*self.__num_cities:(i + 1)*self.__num_cities] = canonical_nodes[::-1][i*self.__num_cities:(i + 1)*self.__num_cities][::-1]
            for i in range(1, self.__num_cities):
                symmetry.append(np.roll(mirror_canonical_nodes, i * self.__num_cities))
            symmetry = np.asarray(symmetry)
        else:
            symmetry = None
        self.machine = machine(hilbert=self.hilbert, symmetry=symmetry, **machine_kwargs)
        self.machine.init_random_parameters(seed=seed)
        self.sampler = sampler(machine=self.machine, d_matrix=self.__d_matrix, **sampler_kwargs)
        self.optimizer = optimizer(self.machine, **optimizer_kwargs)
        self.sr = nk.optimizer.SR(self.machine, diag_shift=diag_shift)
        self.vmc = nk.Vmc(
            hamiltonian=self.hamiltonian,
            sampler=self.sampler,
            optimizer=self.optimizer,
            n_samples=n_samples,
            sr=self.sr,
        )
        super().__init__()

    @property
    def num_cities(self):
        return self.__num_cities

    @property
    def hilbert_size(self):
        return 2 ** self.num_spins

    @property
    def d_matrix(self):
        return self.__d_matrix

    def __build_hamiltonian(self):
        """"""
        # TODO: add function documentation
        sz = np.array([[1, 0], [0, -1]])
        szsz = np.kron(sz, sz)
        # Interaction:
        for α in range(self.num_cities):
            for i in range(self.num_cities):
                for j in range(self.num_cities):
                    self.hamiltonian += nk.operator.LocalOperator(
                        self.hilbert,
                        0.25 * szsz * self.d_matrix[i, j],
                        [
                            self.iα2index(i, α),
                            self.iα2index(j, (α + 1) % self.num_cities),
                        ],
                    )
                    self.hamiltonian += nk.operator.LocalOperator(
                        self.hilbert,
                        0.25 * sz * self.d_matrix[i, j],
                        [self.iα2index(i, α)],
                    )
                    self.hamiltonian += nk.operator.LocalOperator(
                        self.hilbert,
                        0.25 * sz * self.d_matrix[i, j],
                        [self.iα2index(j, (α + 1) % self.num_cities)],
                    )

    def iα2index(self, i, α):
        """"""  # TODO: add documentation
        return i + α * self.num_cities

    def get_relevant_states(
        self, num_states, num_samples=1000, format="L^2", add_true_sol=False
    ):
        """
        This function retrieves the most relevant states from a trained netket
        machine and their corresponding energies.

        Parameters
        ----------
        machine: netket.machine
            A trained machine object from netket.
        sampler: netket.sampler
            A sampler object from netket initialised with a trained machine.
        num_states: int
            A positive integer indicating the number of states to retrieve.
        hamiltonian: netket.operator
            A netket operator which contains the Hamiltonian of a QUBO system.
        num_samples: int
            Positive integer that indicates the number of samples to get
            from the sampler.

        Notes
        -----
        It is assumed that the hilbert space of the problem is a multi-spin system
        """
        # TODO: update documentation

        # Create a bunch of samples (configurations)
        samples = np.array(list(self.sampler.samples(num_samples)))

        samples_r = samples.reshape((-1, samples.shape[-1]))
        unique_samples = lexsort_based(samples_r)

        # Add true solution
        if add_true_sol:
            new_sample = (
                -2 * np.array(np.eye(self.num_cities)).flatten() + 1
            )  # because of netket's sorting of states
            unique_samples = np.vstack((unique_samples, new_sample))

        # Get the wavefunction coefficients
        coeffs = self.machine.log_val(unique_samples)

        # Order configurations according to the energy

        energies = np.array([self.energy(config) for config in unique_samples])
        distances = np.array([self.distance(config) for config in unique_samples])

        sorted_idx = np.argsort(energies)[:num_states]

        unique_samples *= -1  # This is because of netket's sorting

        for config, coeff, energy, dist in zip(
            unique_samples[sorted_idx], coeffs[sorted_idx], energies[sorted_idx], distances[sorted_idx]
        ):
            print("*-.-*-.-*-.-*-.-*-.-*-.-*-.-*-.-*-.-*")
            if format == "L^2":
                print(
                    f"Configuration\n{((config + 1)/2).reshape(self.num_cities, self.num_cities)}"
                )
            else:
                print(f"Configuration {(config + 1)/2}")
            print(f"has relative log coefficient {coeff:.3f}")
            print(f"with energy {energy:.4f} and distance {dist:.4f}.)")

    def energy_from_data(self):
        """"""
        # TODO: finish documentation
        if self.name is not None:
            data = json.load(open(f"{self.name}.log"))
            return data["Output"][-1]["Energy"]["Mean"]
        else:
            return None

    def distance(self, conf):
        """"""
        # TODO: implement documentation
        newconf = self.sampler._kernel.spins2city(conf.reshape(1, -1))[0]
        total_distance = 0
        for i in range(self.num_cities):
            total_distance += self.d_matrix[int(newconf[i]), int(newconf[(i+1)%self.num_cities])]
        return total_distance

    def energy(self, conf):
        """"""
        # TODO: implement documentation
        expected_val = nk.operator.local_values(
            self.hamiltonian, self.machine, conf.reshape(1, -1)
        )[0]
        assert np.abs(np.imag(expected_val)) < 1e-10
        return np.real(expected_val)

    def get_lowest_energy(self):
        """"""
        # TODO: implement documentation

        samples = np.array(list(self.sampler.samples(1000)))

        samples_r = samples.reshape((-1, samples.shape[-1]))
        unique_samples = lexsort_based(samples_r)
        energies = [self.energy(config) for config in unique_samples]
        return min(energies)
    
    def get_lowest_distance(self):
        """"""
        # TODO: implement documentation

        samples = np.array(list(self.sampler.samples(1000)))

        samples_r = samples.reshape((-1, samples.shape[-1]))
        unique_samples = lexsort_based(samples_r)
        distances = [self.distance(config) for config in unique_samples]
        return min(distances)

    def clean_up(self, all=True):
        """"""
        # TODO: finish documentation
        if self.name is not None:
            if all:
                os.remove(f"{self.name}.log")
            os.remove(f"{self.name}.wf")
