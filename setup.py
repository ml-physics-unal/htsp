from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="htsp",
    packages=["htsp", "htsp.solver", "htsp.sampler", "htsp.optuna", "htsp.machine"],
    version='0.0.0',
    description="Library that implements Variational Monte Carlo to solve the Travelling Salesman Problem",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Vladimir Vargas-Calderón and Nicolás Parra",
    author_email="{vvargasc, nparraa}@unal.edu.co",
    license="GNUv3",
    install_requires=["networkx", "numpy==1.19", "optuna", "memory_profiler", "netket[jax] @ git+https://github.com/netket/netket.git@v3.0", "pandas", "neural-tangents"],
    python_requires='>=3.6'
)
