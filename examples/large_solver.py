from htsp.solver import Ising
import numpy as np
import time

L = 10
rows = []
for i in range(L):
    rows.append(np.abs(np.arange(L) - i))
d = np.asarray(rows, dtype=float)
# print(d)

st = time.time()
sol = Ising(
    num_cities=L,
    d_matrix=d,
    sampler_kwargs={"n_chains": 8, "sweep_size": 1, "spins": True, "length": L-1, "totally_random": False},
    n_samples=100,
    machine_kwargs={"alpha": 1,}
)
print(sol.machine.n_par)
sol.run(epochs=1000, baseline=(L-1)*2+0.3)
sol.get_relevant_states(num_states=5, add_true_sol=False)
print(round(time.time() - st), "seconds with RbmSpin")