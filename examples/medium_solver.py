from htsp.solver import NLevels
from htsp.machine import JaxFFNN, JaxCNN
from htsp.sampler import JaxMetropolisLocalExchangeSites
import netket as nk
import numpy as np
import time

L = 12
rows = []
for i in range(L):
    rows.append(np.abs(np.arange(L) - i))
d = np.asarray(rows, dtype=float)
# print(d)

st = time.time()
sol = NLevels(
    num_cities=L,
    d_matrix=d,
    sampler_kwargs={"n_chains": 4, "sweep_size": 1, "spins": False, "length": 10},
    optimizer=nk.optimizer.Sgd,
    n_samples=20,
    machine_kwargs={"n_hidden": 2 * L},
)
sol.run(epochs=1000)
sol.get_relevant_states(num_states=5, add_true_sol=False)

print(round(time.time() - st), "seconds with RbmMultiVal")

st = time.time()
sol = NLevels(
    num_cities=L,
    d_matrix=d,
    sampler=JaxMetropolisLocalExchangeSites,
    sampler_kwargs={"n_chains": 4, "sweep_size": 1, "spins": False, "length": 10},
    optimizer=nk.optimizer.Sgd,
    optimizer_kwargs={"learning_rate": 0.1},
    machine=JaxFFNN,
    machine_kwargs={"n_hidden": 2 * L, "n_layers": 1},
    n_samples=20,
)
sol.run(epochs=1000)
sol.get_relevant_states(num_states=5, add_true_sol=False)

print(round(time.time() - st), "seconds with JaxFFNN")

st = time.time()
sol = NLevels(
    num_cities=L,
    d_matrix=d,
    sampler=JaxMetropolisLocalExchangeSites,
    sampler_kwargs={"n_chains": 4, "sweep_size": 1, "spins": False, "length": L-2},
    optimizer=nk.optimizer.Sgd,
    optimizer_kwargs={"learning_rate": 0.1},
    machine=JaxCNN,
    machine_kwargs={"n_channels": 4, "n_layers": 2, "filter_size": 3, "stride": 2},
    n_samples=20,
)
sol.run(epochs=10000, patience=300, baseline=(L-1)*2+0.3)
sol.get_relevant_states(num_states=5, add_true_sol=False)

print(round(time.time() - st), "seconds with JaxCNN")
